# ShortCut
[安裝](#安裝)

[執行](#執行)

[建圖與校正](#建圖與校正)

[關於UI](#關於ui)


# 安裝 
## Do following things on "vehicle"
1. Extract the zip file to ~/Documents
2. edit file ~/Documents/ITRI_MR_*/ITRIMR/build/agent_configuration.json to correct setting
3. move the agent_configuration.json to /opt/itri/mr/etc

#### hint(copy insturction):
```
sudo mkdir -p /opt/itri/mr/etc

cd /opt/itri/mr/etc

sudo cp ~/Documents/ITRI_MR_${版本號}/ITRIMR/build/agent_configuration.json .

# ${版本號}=0917_v1（資料庫版本）
# ${版本號}=0913_v1（無資料庫版本）
```

## Install the spil_ds_simulation(fork by yes5528876) on LS-series "vehicle"
1. edit the file in ~/Documents/spil_ds_simulation/server/config/configuration.json. Note that you just need to edit the "mr1" section.
2. prepare the libraries
```
cd ~/Documents/soter_msgs
mkdir build
cd build
cmake ..
make
source ~/Documents/soter_msgs/build/catkin_generated/installspace/setup.bash
```
3. build spil_ds_simulation
```
cd ~/Documents/spil_ds_simulation/scripts
source buildSimulator.sh
cd ~/Documents/spil_ds_simulation/scripts
source buildSimulator.sh
```
```
hint:(若build失敗則執行此區塊)
在這步驟可能會因為複製過來的檔案殘存上個電腦的路徑
，故把build資料夾刪掉就好。

要刪掉的build資料夾：
spil_ds_simulation下的itri_ds_robot_msgs & itri_mr_simple_controller & server 下的build

以及~/Documents/libgyrocml下的build
```

## On "main computer"
1. move the ITRI_DS_0916_v1 folder to ~/Documents
2. move the ITRI_MCS folder to ~/Documents


# 執行
[在無連接資料庫情況下執行](#在無連接資料庫情況下執行)

[在有連接資料庫情況下執行](#在有連接資料庫情況下執行)
## 在無連接資料庫情況下執行
### open Dispatching System on the main computer
1. source gyromr/scripts/testITRIMR.sh

### open the programs needed on LS-series vehicle 
1. The following ls??? means the type of LS-series vehicle, e.g. ls250 or ls100
2. roslaunch ls???_bringup ls???.launch
3. roslaunch ls???_navigation ls???_nav.launch
4. select the right map in rviz(square2.yaml in this case), and press the global search button to do localization

    4.a. If you want to add new map, just put your map into catkin_ws/src/map_manager_server/map folder

5. Write following code block to ~/.bashrc(注意：請把以下code放在.bashrc的最後面)
```
source /opt/ros/dashing/setup.bash;

source ~/Documents/alice/ROS2Projects/universal_interface/install/setup.bash

source ~/Documents/alice/ROS2Projects/map_interface/install/setup.bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/Documents/ITRILibs
```
6. You need to change the ROSID in ITRI_MR_0913_v1/scripts/runMR.sh to the same as acs (default:12)
7. source ITRI_MR_0913_v1/scripts/runMR.sh
8. reset and auto in MRServer

---
---
---
---
---
## 在有連接資料庫情況下執行
### On main computer
1. 
```
source ~/Documents/ITRI_DS_0916_v1/scripts/runDS.sh
source ~/Documents/ITRI_MCS/scripts/runMCS.sh 
```

2. In the DBeaver/transfer_list/transfer_state, you should remove all "Queued" state elements before start.

3. Go to localhost and localhost:8888 in your browser.
```
username: user
password: qagv
```

## On LS-series vehicle 
1. The following ls??? means the type of LS-series vehicle, e.g. ls250 or ls100
2. roslaunch ls???_bringup ls???.launch
3. roslaunch ls???_navigation ls???_nav.launch
4. select the right map in rviz(square2.yaml in this case), and press the global search button to do localization

    4.a. If you want to add new map, just put your map into catkin_ws/src/map_manager_server/map folder

5. Write following code block to ~/.bashrc(注意：請把以下code放在.bashrc的最後面)
```
source /opt/ros/dashing/setup.bash;

source /home/itrimr/Documents/alice/ROS2Projects/universal_interface/install/setup.bash

source /home/itrimr/Documents/alice/ROS2Projects/map_interface/install/setup.bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/Documents/ITRILibs
```
6. You need to change the ROSID in ITRI_MR_0917_v1/scripts/runMR.sh to the same as acs (default:12)
7. source ITRI_MR_0917_v1/scripts/runMR.sh
8. reset->auto->reset in MRServer

## On main computer
```
cd ~/Documents/ITRI_MCS/build/bin/
./test_easy_send_xml commands.txt 0 SIMMOVEONLY

```


# 建圖與校正

## 建立地圖
1. $ roslaunch ls???_bringup ls???.launch
2. $ roslaunch ls???_navigation build_map.launch 
3. Go around the region that you want to build map
4. $ rosrun map_server map_saver -f name_of_map
5. put the .pgm & .yaml file to the map_manager package's map folder


## 校正座標
1. find an arbitrary point in graph.josn as the base point
2. bringup, navigation, and load the map
3. move the robot to the base point
3. $ rostopic echo /soter_local_localization_node/robot_pose
4. adjust the .yaml file and make the robot_pose consistent with the graph.json

# 關於UI
1. config file is /var/www/html/api-agv/db.php
